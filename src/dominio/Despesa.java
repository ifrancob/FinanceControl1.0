package dominio;

public class Despesa {

	private int Id;
	private String descricao;
	private double valor;
	private java.sql.Date data;
	private String origem;
	private int nParcelas;


	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public java.sql.Date getData() {
		return data;
	}
	public void setData(java.sql.Date data) {
		this.data = data;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public int getnParcelas() {
		return nParcelas;
	}
	public void setnParcelas(int nParcelas) {
		this.nParcelas = nParcelas;
	}


}

