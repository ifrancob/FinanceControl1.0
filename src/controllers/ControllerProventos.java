package controllers;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import dominio.Despesa;
import dominio.Provento;
import models.dao.DaoDespesa;
import models.dao.DaoProvento;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import javafx.scene.control.Separator;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Accordion;

import javafx.scene.control.TextField;

import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Label;

import javafx.scene.control.ScrollPane;

import javafx.scene.layout.AnchorPane;

import javafx.scene.control.CheckBox;

import javafx.scene.control.DatePicker;
import util.*;

public class ControllerProventos implements Initializable {

	private DaoProvento DAOprov = new DaoProvento();
	private DaoDespesa DAOdesp = new DaoDespesa();

	@FXML
	private MaskTextField txtfPValor;
	@FXML
	private TextField txtfDValor;

	@FXML
	private Button btnInserir;
	@FXML
	private Button btnEditar;
	@FXML
	private Button btnDeletar;
	@FXML
	private Button btnPesquisar;
	@FXML
	private Button btnCancelar;
	@FXML
	private Button btnListarProventos;
	@FXML
	private Button btnListarDespesas;
	@FXML
	private Separator separador;
	@FXML
	private Accordion accordion;
	@FXML
	private TitledPane TitledPane;
	@FXML
	private AnchorPane acProventos;
	@FXML
	private TextField txtfPDescricao;

	@FXML
	private TextField txtfPOrigem;
	@FXML
	private DatePicker dtData;
	@FXML
	private TextField txtfDLocal;
	@FXML
	private Label lblAlert;
	@FXML
	private CheckBox cbPPeriodico;
	@FXML
	private MaskTextField txtfPPeriodica;
	@FXML
	private ScrollPane scrollPane;
	@FXML
	private Separator separador1;

	@FXML
	private DatePicker dtDData;
	@FXML
	private Label lblDDescricao;
	@FXML
	private Label lblTitulo;
	@FXML
	private Label lblDValor;
	@FXML
	private Label lblDData;
;
	@FXML
	private CheckBox cbDPeriodico;
	@FXML
	private MaskTextField txtfDPeriodica;
	@FXML
	private TitledPane paneDespesas;
	@FXML
	private AnchorPane acDespesas;
	@FXML
	private TitledPane paneProventos;
	@FXML
	private TableView<Object> resultsTb = new TableView<>();
	@FXML
	private TableColumn<Provento, Integer> cellId = new TableColumn<>("Id");
	@FXML
	private TableColumn<Provento, String> cellDescricao = new TableColumn<>("Descricao");
	@FXML
	private TableColumn<Provento, Double> cellValor = new TableColumn<>("valor");
	@FXML
	private TableColumn<Provento, Date> cellData = new TableColumn<>("data");
	@FXML
	private TableColumn<Provento, String> cellOrigem = new TableColumn<>("origem");

	public void inserir() {
		if (lblTitulo.getText().equals("Proventos")) {



			if (verificaCamposProvento()) {
				Provento provento = new Provento();
				paneProventos.expandedProperty().setValue(true);

				provento.setDescricao(txtfPDescricao.getText());
				provento.setValor(Double.parseDouble(txtfPValor.getText()));

				LocalDate dataLocal = dtData.getValue();
				provento.setData(java.sql.Date.valueOf(dataLocal));
				provento.setOrigem(txtfPOrigem.getText());

				DAOprov.inserir(provento);

				listar("Proventos");
				limparForm("Proventos");

			} else {
				lblAlert.setText("*Digite todos os campos marcados!");
			}
		}

		else {
			if (verificaCamposDespesa()) {
				Despesa despesa = new Despesa();
				paneDespesas.expandedProperty().setValue(true);

				despesa.setDescricao(txtfDLocal.getText());
				despesa.setValor(Double.parseDouble(txtfDValor.getText()));

				LocalDate dataLocal = dtDData.getValue();
				despesa.setData(java.sql.Date.valueOf(dataLocal));

				DAOdesp.inserir(despesa);

				listar("Despesas");
				limparForm("Despesas");
			} else {
				lblAlert.setText("*Digite todos os campos marcados!");
			}
		}
	}

	public void pesquisar() {
		if (lblTitulo.getText().equals("Proventos")) {
			ArrayList<Provento> results = DAOprov.pesquisar(txtfPDescricao.getText());
			if (txtfPDescricao.getText().isEmpty()) {
				lblAlert.setText("*Digite o valor a ser pesquisado no campo 'Descri��o'");
			} else {
				lblAlert.setText("");

				if (results.size() == 0) {
					resultsTb.setPlaceholder(new Label("Nenhum resultado encontrado"));
				}
				ObservableList<Object> resultsList = FXCollections.observableArrayList();
				resultsList.addAll(results);

				resultsTb.setItems(resultsList);
				popularCelulas();

				limparForm("Proventos");
			}
		} else if (lblTitulo.getText().equals("Despesas")) {
			ArrayList<Despesa> results = DAOdesp.pesquisar(txtfDLocal.getText());
			if (txtfDLocal.getText().isEmpty()) {
				lblAlert.setText("*Digite o valor a ser pesquisado no campo 'Local'");
			} else {
				lblAlert.setText("");
				if (results.size() == 0) {
					resultsTb.setPlaceholder(new Label("Nenhum resultado encontrado"));
				}
				ObservableList<Object> resultsList = FXCollections.observableArrayList();
				resultsList.addAll(results);

				resultsTb.setItems(resultsList);
				popularCelulas();

				limparForm("Despesas");
			}
		}

	}

	public void delete() {
		if(btnInserir.isDisable()){
			btnInserir.setDisable(false);
			btnPesquisar.setDisable(false);
		}


		if (lblTitulo.getText().equals("Proventos")) {
			ObservableList<Object> result = resultsTb.getSelectionModel().getSelectedItems();
			DAOprov.excluir((Provento) result.get(0));
			listar("Proventos");
			limparForm("Proventos");
		} else {
			ObservableList<Object> result = resultsTb.getSelectionModel().getSelectedItems();
			DAOdesp.excluir((Despesa) result.get(0));
			listar("Despesas");
			limparForm("Despesas");
		}
	}

	public void editar() {
		ObservableList<Object> result = resultsTb.getSelectionModel().getSelectedItems();

		if (lblTitulo.getText().equals("Proventos")) {
			if (btnEditar.getText().equals("Editar")) {

				paneProventos.expandedProperty().setValue(true);

				btnInserir.setDisable(true);
				btnPesquisar.setDisable(true);
				txtfPDescricao.setText(((Provento) result.get(0)).getDescricao());
				txtfPOrigem.setText(((Provento) result.get(0)).getOrigem());
				txtfPValor.setText(String.valueOf(((Provento) result.get(0)).getValor()));

				Date data = (Date) ((Provento) result.get(0)).getData();
				LocalDate dataLocal = data.toLocalDate();
				dtData.setValue(dataLocal);
				btnEditar.setText("Salvar");

			} else {

				Provento novoProvento = new Provento();
				novoProvento.setDescricao(txtfPDescricao.getText());
				novoProvento.setValor(Double.parseDouble(txtfPValor.getText()));

				LocalDate dataLocal = dtData.getValue();
				novoProvento.setData(java.sql.Date.valueOf(dataLocal));
				novoProvento.setOrigem(txtfPOrigem.getText());

				DAOprov.editar(((Provento) result.get(0)).getId(), novoProvento);
				btnEditar.setText("Editar");

				btnInserir.setDisable(false);
				btnPesquisar.setDisable(false);

				limparForm("Proventos");
				listar("Proventos");

			}
		} else {
			if (btnEditar.getText().equals("Editar")) {

				paneDespesas.expandedProperty().setValue(true);

				btnInserir.setDisable(true);
				btnPesquisar.setDisable(true);

				txtfDLocal.setText(((Despesa) result.get(0)).getDescricao());
				txtfDValor.setText(String.valueOf(((Despesa) result.get(0)).getValor()));

				Date data = (Date) ((Despesa) result.get(0)).getData();
				LocalDate dataLocal = data.toLocalDate();
				dtDData.setValue(dataLocal);
				btnEditar.setText("Salvar");

			} else {

				Despesa novaDespesa = new Despesa();
				novaDespesa.setDescricao(txtfDLocal.getText());
				novaDespesa.setValor(Double.parseDouble(txtfDValor.getText()));

				LocalDate dataLocal = dtDData.getValue();
				novaDespesa.setData(java.sql.Date.valueOf(dataLocal));

				DAOdesp.editar(((Despesa) result.get(0)).getId(), novaDespesa);

				btnEditar.setText("Editar");
				btnInserir.setDisable(false);
				btnPesquisar.setDisable(false);

				limparForm("Despesas");
				listar("Despesas");

			}
		}
	}
	public void cancelar(){

		limparForm("Proventos");
		limparForm("Despesas");
		btnInserir.setText("Inserir");
		btnInserir.setDisable(false);

		btnEditar.disableProperty().unbind();
		btnEditar.setDisable(true);
		btnDeletar.disableProperty().unbind();
		btnDeletar.setDisable(true);
		btnPesquisar.setDisable(false);
		resultsTb.getSelectionModel().clearSelection();
		if(lblTitulo.getText().equals("Proventos")){
			listar("Proventos");
		}else if (lblTitulo.getText().equals("Despesas")){
			listar("Despesas");
		}
	}

	public void popularCelulas() {
		cellId.setCellValueFactory(new PropertyValueFactory<>("Id"));
		cellDescricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));
		cellValor.setCellValueFactory(new PropertyValueFactory<>("valor"));
		cellData.setCellValueFactory(new PropertyValueFactory<>("data"));
		cellOrigem.setCellValueFactory(new PropertyValueFactory<>("origem"));

	}

	public void listar(String valor) {

		if(btnInserir.getText().equals("Salvar")){
			btnInserir.setText("Editar");
		}
			btnInserir.setDisable(false);
			btnPesquisar.setDisable(false);

		lblTitulo.setText(valor);
		lblAlert.setText("");

		if (valor.equals("Proventos")) {
			limparForm("Despesas");
			limparForm("Proventos");
			cellOrigem.setVisible(true);
			cellDescricao.setText("Descri��o");
			paneProventos.expandedProperty().setValue(true);

			ArrayList<Provento> resultsProventos = DAOprov.pesquisarAll();
			if (resultsProventos.size() == 0) {
				resultsTb.setPlaceholder(new Label("Nenhum Provento adicionado"));
			}

			ObservableList<Object> resultsList = FXCollections.observableArrayList();
			resultsList.addAll(resultsProventos);

			resultsTb.setItems(resultsList);
			popularCelulas();

		} else {
			limparForm("Proventos");
			limparForm("Despesas");
			cellDescricao.setText("Local");
			cellOrigem.setVisible(false);
			paneDespesas.expandedProperty().setValue(true);

			ArrayList<Despesa> resultsDespesas = DAOdesp.pesquisarAll();
			if (resultsDespesas.size() == 0) {
				resultsTb.setPlaceholder(new Label("Nenhuma Despesa adicionada"));
			}
			ObservableList<Object> resultsList = FXCollections.observableArrayList();
			resultsList.addAll(resultsDespesas);

			resultsTb.setItems(resultsList);

			popularCelulas();
		}

	}

	public void limparForm(String valor) {
		// limpar forms proventos
		if (valor.equals("Proventos")) {
			txtfPDescricao.clear();
			txtfPValor.clear();
			txtfPOrigem.clear();
			dtData.setValue(null);
			cbPPeriodico.setSelected(false);
			txtfPPeriodica.clear();
			txtfPPeriodica.setDisable(true);
		}
		// limpar forms despesa
		if (valor.equals("Despesas")) {

			txtfDLocal.clear();
			txtfDValor.clear();
			dtDData.setValue(null);
			cbDPeriodico.setSelected(false);
			txtfDPeriodica.clear();
			txtfDPeriodica.setDisable(true);
		}

		btnEditar.setText("Editar");

	}
	public void habilitarPeriodico(String valor){

		if(valor.equals("Proventos")){
			txtfPPeriodica.setDisable(false);
			if(!cbPPeriodico.isSelected()){
				txtfPPeriodica.setDisable(true);
			}
		}else{
			txtfDPeriodica.setDisable(false);
			if(!cbDPeriodico.isSelected()){
				txtfDPeriodica.setDisable(true);
			}

		}
	}


	public boolean verificaCamposProvento() {

		if (txtfPDescricao.getText().isEmpty() || txtfPValor.getText().isEmpty() || dtData.getValue() == null) {
			return false;
		}

		return true;
	}

	public boolean verificaCamposDespesa() {

		if (txtfDLocal.getText().isEmpty() || txtfDValor.getText().isEmpty() || dtDData.getValue() == null) {
			return false;
		}

		return true;
	}

	// Controle das abas selecionadas
	public void abaControlProventos() {

		if(btnInserir.getText().equals("Salvar")){
			btnInserir.setText("Editar");
		}
			btnInserir.setDisable(false);
			btnPesquisar.setDisable(false);
			cellOrigem.setVisible(true);
			cellDescricao.setText("Descri��o");
		limparForm("Proventos");
		listar("Proventos");
		lblAlert.setText("");
	}

	public void abaControlDespesa() {
		if(btnInserir.getText().equals("Salvar")){
			btnInserir.setText("Editar");
			}
			cellDescricao.setText("Local");
			cellOrigem.setVisible(false);
			limparForm("Despesas");
			btnInserir.setDisable(false);
			btnPesquisar.setDisable(false);


		listar("Despesas");
		lblAlert.setText("");

	}

	public void limparMsg() {
		btnDeletar.disableProperty().bind(resultsTb.getSelectionModel().selectedItemProperty().isNull());
		btnEditar.disableProperty().bind(resultsTb.getSelectionModel().selectedItemProperty().isNull());
		lblAlert.setText("");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		btnInserir.setOnMouseClicked(e -> inserir());
		btnPesquisar.setOnMouseClicked(e -> pesquisar());
		btnEditar.setOnMouseClicked(e -> editar());
		btnListarProventos.setOnMouseClicked(e -> listar("Proventos"));
		btnListarDespesas.setOnMouseClicked(e -> listar("Despesas"));
		btnDeletar.setOnMouseClicked(e -> delete());

		paneProventos.setOnMouseClicked(e -> abaControlProventos());
		paneDespesas.setOnMouseClicked(e -> abaControlDespesa());
		cbDPeriodico.setOnMouseClicked(e->habilitarPeriodico("Despesas"));
		cbPPeriodico.setOnMouseClicked(e->habilitarPeriodico("Proventos"));
		// Mensagem apresentada na tabela inicial
		resultsTb.setPlaceholder(new Label("Selecione alguma categoria ao lado"));

		resultsTb.setOnMouseClicked(e -> limparMsg());

		btnDeletar.disableProperty().bind(resultsTb.getSelectionModel().selectedItemProperty().isNull());
		btnEditar.disableProperty().bind(resultsTb.getSelectionModel().selectedItemProperty().isNull());
		btnCancelar.setOnMouseClicked(e->cancelar());

		txtfPValor.setMask("N+.NN");
		txtfPPeriodica.setMask("N+");
		txtfDPeriodica.setMask("N+");
	}

}
