package models.dao;

import dominio.Provento;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;



public class DaoProvento{
	private String SQLcommand;
	private PreparedStatement statement;
	private ResultSet results;
	private Connection conn = FactoryConnection.getConnection();



	public void inserir(Provento provento) {

		this.SQLcommand = "INSERT INTO financecontrol.proventos(descricao, valor, data, origem)"
				+ " VALUES(?,?,?,?)";
		try {
			this.statement = conn.prepareStatement(SQLcommand);
			statement.setString(1, provento.getDescricao());
			statement.setDouble(2, provento.getValor());
			statement.setDate(3, (java.sql.Date) provento.getData());
			statement.setString(4, provento.getOrigem());
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public void excluir(Provento provento) {

		this.SQLcommand = "DELETE FROM financecontrol.proventos WHERE id=?";
		try {
			this.statement = conn.prepareStatement(SQLcommand);
			statement.setInt(1, provento.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public ArrayList<Provento> pesquisar(String descricao) {

		this.SQLcommand = "SELECT id, descricao, valor, data, origem  FROM financecontrol.proventos WHERE descricao = ?";
		ArrayList<Provento> proventosRs = new ArrayList<>();
		try {
			this.statement = conn.prepareStatement(SQLcommand);
			this.statement.setString(1, descricao);
			this.results = this.statement.executeQuery();

			while(results.next()){
				Provento proventoTmp = new Provento();
				proventoTmp.setId(results.getInt(1));
				proventoTmp.setDescricao(results.getString(2));
				proventoTmp.setValor(results.getDouble(3));
				proventoTmp.setData(results.getDate(4));
				proventoTmp.setOrigem(results.getString(5));

				proventosRs.add(proventoTmp);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return proventosRs;
	}

	public ArrayList<Provento> pesquisarAll(){
		this.SQLcommand = "SELECT * FROM financecontrol.proventos";
		ArrayList<Provento> proventosRs = new ArrayList<>();
		try {
			this.statement = conn.prepareStatement(SQLcommand);
			this.results = this.statement.executeQuery();

			while(results.next()){
				Provento proventoTmp = new Provento();
				proventoTmp.setId(results.getInt(1));
				proventoTmp.setDescricao(results.getString(2));

				proventoTmp.setData(results.getDate(3));
				proventoTmp.setOrigem(results.getString(4));
				proventoTmp.setValor(results.getDouble(5));
				proventosRs.add(proventoTmp);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return proventosRs;

	}

	public void editar(int id, Provento novoProvento) {
		this.SQLcommand = "UPDATE financecontrol.proventos SET descricao = ?, valor = ?, data = ?, origem = ? WHERE id = ?";
		try {

			this.statement = conn.prepareStatement(SQLcommand);
			statement.setString(1, novoProvento.getDescricao());
			statement.setDouble(2, novoProvento.getValor());
			statement.setDate(3, (java.sql.Date)novoProvento.getData());
			statement.setString(4, novoProvento.getOrigem());
			statement.setInt(5, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}

