package models.dao;

import dominio.Despesa;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;



public class DaoDespesa{
	private String SQLcommand;
	private PreparedStatement statement;
	private ResultSet results;
	private Connection conn = FactoryConnection.getConnection();



	public void inserir(Despesa despesa) {

		this.SQLcommand = "INSERT INTO financecontrol.despesas(descricao, valor, data, origem)"
				+ " VALUES(?,?,?,?)";
		try {
			this.statement = conn.prepareStatement(SQLcommand);
			statement.setString(1, despesa.getDescricao());
			statement.setDouble(2, despesa.getValor());
			statement.setDate(3, (java.sql.Date) despesa.getData());
			statement.setString(4, despesa.getOrigem());
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public void excluir(Despesa despesa) {

		this.SQLcommand = "DELETE FROM financecontrol.despesas WHERE id=?";
		try {
			this.statement = conn.prepareStatement(SQLcommand);
			statement.setInt(1, despesa.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public ArrayList<Despesa> pesquisar(String descricao) {

		this.SQLcommand = "SELECT id, descricao, valor, data, origem  FROM financecontrol.despesas WHERE descricao = ?";
		ArrayList<Despesa> despesasRs = new ArrayList<>();
		try {
			this.statement = conn.prepareStatement(SQLcommand);
			this.statement.setString(1, descricao);
			this.results = this.statement.executeQuery();

			while(results.next()){
				Despesa despesaTmp = new Despesa();
				despesaTmp.setId(results.getInt(1));
				despesaTmp.setDescricao(results.getString(2));
				despesaTmp.setValor(results.getDouble(3));
				despesaTmp.setData(results.getDate(4));
				despesaTmp.setOrigem(results.getString(5));
				despesasRs.add(despesaTmp);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return despesasRs;
	}

	public ArrayList<Despesa> pesquisarAll(){
		this.SQLcommand = "SELECT * FROM financecontrol.despesas";
		ArrayList<Despesa> despesasRs = new ArrayList<>();
		try {
			this.statement = conn.prepareStatement(SQLcommand);
			this.results = this.statement.executeQuery();

			while(results.next()){
				Despesa despesaTmp = new Despesa();
				despesaTmp.setId(results.getInt(1));
				despesaTmp.setDescricao(results.getString(2));
				despesaTmp.setData(results.getDate(3));
				despesaTmp.setOrigem(results.getString(4));
				despesaTmp.setValor(results.getDouble(5));
				despesasRs.add(despesaTmp);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return despesasRs;

	}

	public void editar(int id, Despesa novaDespesa) {
		this.SQLcommand = "UPDATE financecontrol.despesas SET descricao = ?, valor = ?, data = ?, origem = ? WHERE id = ?";
		try {

			this.statement = conn.prepareStatement(SQLcommand);
			statement.setString(1, novaDespesa.getDescricao());
			statement.setDouble(2, novaDespesa.getValor());
			statement.setDate(3, (java.sql.Date)novaDespesa.getData());
			statement.setString(4, novaDespesa.getOrigem());
			statement.setInt(5, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}

