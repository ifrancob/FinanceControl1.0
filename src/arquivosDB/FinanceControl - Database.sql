create database FinanceControl;

create table FinanceControl.Proventos(
	id int not null auto_increment,
    descricao char(70),
    data date,
    origem char(50),
    valor double,
    primary key(id)
    );
    
    
create table FinanceControl.Despesas(
	id int not null auto_increment,
    descricao char(70),
    data date,
    origem char(50),
    valor double,
    primary key(id)
    );
    